{
	"android": {
		"mac": "40:F3:85:90:BE:D0",
		"maclist": [
			"D3:B2:4E:DA:24:A6",
			"DA:A9:42:A1:61:8F",
			"40:F3:85:90:BF:EF",
			"E5:6C:D9:E6:78:EE",
			"DE:BD:F3:18:1F:A3"
		],
		"rssi1": -900,
		"rssi2": -100,
		"rssi3": -78
	},
	"apple": {
		"uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647827",
		"major": 10003,
		"minor": 34452,
		"rssi1": -900,
		"rssi2": -100,
		"rssi3": -78
	}
}